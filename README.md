# M998 Series 1.25-Ton 4x4 High Mobility Multipurpose Wheeled Vehicle (HMMWV)

This repository contains my curated cache of technical manuals for the M998 Series 1.25-Ton 4x4 High Mobility Multipurpose Wheeled Vehicle (HMMWV). This repository covers earlier vehicles which found their way into surplus sales by around 2000, but not later variants which have been surplussed more recently.

![M1097 Line Drawing](M1097.png)

Vehicles in the series include:

Name                                                                            | Model   | NSN
--------------------------------------------------------------------------------|---------|-----------------
TRUCK, UTILITY: CARGO/TROOP CARRIER, 1-1/4 TON, 4X4                             | M998    | 2320-01-107-7155
TRUCK, UTILITY: CARGO/TROOP CARRIER, 1-1/4 TON, 4X4                             | M998A1  | 2320-01-371-9577
TRUCK, UTILITY: CARGO/TROOP CARRIER, 1-1/4 TON, 4X4, W/WINCH                    | M1038   | 2320-01-107-7156
TRUCK, UTILITY: CARGO/TROOP CARRIER, 1-1/4 TON, 4X4, W/WINCH                    | M1038A1 | 2320-01-371-9578
TRUCK, UTILITY: HEAVY VARIANT, 4X4                                              | M1097   | 2320-01-346-9317
TRUCK, UTILITY: HEAVY VARIANT, 4X4                                              | M1097A1 | 2320-01-371-9583
TRUCK, UTILITY: HEAVY VARIANT, 4X4                                              | M1097A2 | 2320-01-380-8604
TRUCK, UTILITY: HEAVY VARIANT, 4X4                                              | M1123   | 2320-01-455-9593
TRUCK, UTILITY: TOW CARRIER, ARMORED, 1-1/4 TON, 4X4                            | M966    | 2320-01-107-7153
TRUCK, UTILITY: TOW CARRIER, ARMORED, 1-1/4 TON, 4X4                            | M966A1  | 2320-01-372-3932
TRUCK, UTILITY: TOW CARRIER, ARMORED, 1-1/4 TON, 4X4                            | M1121   | 2320-01-956-1282
TRUCK, UTILITY: TOW CARRIER, ARMORED, 1-1/4 TON, 4X4, W/WINCH                   | M1036   | 2320-01-107-7154
TRUCK, UTILITY: TOW CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4               | M1045   | 2320-01-146-7191
TRUCK, UTILITY: TOW CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4               | M1045A1 | 2320-01-371-9580
TRUCK, UTILITY: TOW CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4               | M1045A2 | 2320-01-380-8229
TRUCK, UTILITY: TOW CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4, W/WINCH      | M1046   | 2320-01-146-7188
TRUCK, UTILITY: TOW CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4, W/WINCH      | M1046A1 | 2320-01-371-9582
TRUCK, UTILITY: ARMAMENT CARRIER, ARMORED, 1-1/4 TON, 4X4                       | M1025   | 2320-01-128-9551
TRUCK, UTILITY: ARMAMENT CARRIER, ARMORED, 1-1/4 TON, 4X4                       | M1025A1 | 2320-01-371-9584
TRUCK, UTILITY: ARMAMENT CARRIER, ARMORED, 1-1/4 TON, 4X4                       | M1025A2 | 2320-01-380-8233
TRUCK, UTILITY: ARMAMENT CARRIER, ARMORED, 1-1/4 TON, 4X4, W/WINCH              | M1026   | 2320-01-128-9552
TRUCK, UTILITY: ARMAMENT CARRIER, ARMORED, 1-1/4 TON, 4X4, W/WINCH              | M1026A1 | 2320-01-371-9579
TRUCK, UTILITY: ARMAMENT CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4          | M1043   | 2320-01-146-7190
TRUCK, UTILITY: ARMAMENT CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4          | M1043A1 | 2320-01-372-3933
TRUCK, UTILITY: ARMAMENT CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4          | M1043A2 | 2320-01-380-8213
TRUCK, UTILITY: ARMAMENT CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4, W/WINCH | M1044   | 2320-01-146-7189
TRUCK, UTILITY: ARMAMENT CARRIER, W/SUPPLEMENTAL ARMOR, 1-1/4 TON, 4X4, W/WINCH | M1044A1 | 2320-01-371-9581
TRUCK, UTILITY: S250 SHELTER CARRIER, 4X4                                       | M1037   | 2320-01-146-7193
TRUCK, UTILITY: S250 SHELTER CARRIER, 4X4, W/WINCH                              | M1042   | 2320-01-146-7187
TRUCK, AMBULANCE, 2-LITTER, ARMORED, 4X4                                        | M996    | 2310-01-111-2275
TRUCK, AMBULANCE, 2-LITTER, ARMORED, 4X4                                        | M996A1  | 2310-01-372-3935
TRUCK, AMBULANCE, 4-LITTER, ARMORED, 4X4                                        | M997    | 2310-01-111-2274
TRUCK, AMBULANCE, 4-LITTER, ARMORED, 4X4                                        | M997A1  | 2310-01-372-3934
TRUCK, AMBULANCE, 4-LITTER, ARMORED, 4X4                                        | M997A2  | 2310-01-380-8225
TRUCK, AMBULANCE, 2-LITTER, SOFT TOP, 4X4                                       | M1035   | 2310-01-146-7194
TRUCK, AMBULANCE, 2-LITTER, SOFT TOP, 4X4                                       | M1035A1 | 2310-01-371-9585
TRUCK, AMBULANCE, 2-LITTER, SOFT TOP, 4X4                                       | M1035A2 | 2310-01-380-8290

![M998 Series Line Drawings](M998_Series.png)


The documents are sorted into these subdirectories:


Directory              | Description
-----------------------|---------------------------------------
[TM](TM/README.md)     | Official US military Technical Manuals
[P2P](P2P/README.md)   | Point-to-point wiring diagram program
