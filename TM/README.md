# M998 Series Technical Manuals

This directory contains official technical manuals for the M998 series trucks.


## Truck Technical Manuals

Title                                  | Manual                                          | Date
---------------------------------------|-------------------------------------------------|-----------
Operator's Manual                      | [TM 9-2320-280-10](TM9-2320-280-10.pdf)         | 2004-07-15
Hand Receipts                          | [TM 9-2320-280-10-HR](TM9-2320-280-10-HR.pdf)   | 2004-07-15
Unit Maintenance, Volume 1             | [TM 9-2320-280-20-1](TM9-2320-280-20-1.pdf)     | 2004-07-15
Unit Maintenance, Volume 2             | [TM 9-2320-280-20-2](TM9-2320-280-20-2.pdf)     | 2004-07-15
Unit Maintenance, Volume 3             | [TM 9-2320-280-20-3](TM9-2320-280-20-3.pdf)     | 2004-07-15
Direct and General Support Maintenance | [TM 9-2320-280-34](TM9-2320-280-34.pdf)         | 2004-07-15
Parts Manual, Volume 1                 | [TM 9-2320-280-24P-1](TM9-2320-280-24P-1.pdf)   | 2004-07-15
Parts Manual, Volume 2                 | [TM 9-2320-280-24P-2](TM9-2320-280-24P-2.pdf)   | 2004-07-15
Lubrication Order                      | [LO 9-2320-280-12](LO9-2320-280-12.pdf)         | 1990-06-19
Transportability Guidance              | [TM 55-2320-280-14](TM55-2320-280-14.pdf)       | 1988-08-25


## Component Technical Manuals

Title                                  | Manual                                          | Date
---------------------------------------|-------------------------------------------------|-----------
Engine Maintenance Manual              | [TM 9-2815-237-34](TM9-2815-237-34.pdf)         | 2004-07-15
Engine Parts Manual                    | [TM 9-2815-237-34P](TM9-2815-237-34P.pdf)       | 2004-07-15
  

## Modification Work Orders

Title                                  | Manual                                          | Date
---------------------------------------|-------------------------------------------------|-----------
Power Steering Systems                 | [MWO 9-2320-280-20-1](MWO9-2320-280-20-1.pdf)   | 1993-05-24
Crossmember Reinforcement              | [MWO 9-2320-280-20-7](MWO9-2320-280-20-7.pdf)   | 2001-12-01
Dual Service Parking Brake             | [MWO 9-2320-280-35-1](MWO9-2320-280-35-1.pdf)   | 1990-03-30
3-Point Seatbelts                      | [MWO 9-2320-280-35-2](MWO9-2320-280-35-2.pdf)   | 1996-06-01


## Installation Manuals

Title                                               | Manual                                          | Date
----------------------------------------------------|-------------------------------------------------|-----------
Add-On-Armor Air Conditioning                       | [TB 9-2510-251-13](TB9-2510-251-13.pdf)         | 2007-08-31
MK-2325/VRC for AN/VRC-87/88/90                     | [TB 11-5820-890-20-27](TB11-5820-890-20-27.pdf) | 1999-09-01
MK-2326/VRC for AN/VRC-89/91/92                     | [TB 11-5820-890-20-28](TB11-5820-890-20-28.pdf) | 1999-09-01
MK-2327/VRC for Dual AN/VRC-89/91/92                | [TB 11-5820-890-20-29](TB11-5820-890-20-29.pdf) | 2000-06-01
MK-2328/VRC for AN/VRC-87/88/90 and AN/VRC-89/91/92 | [TB 11-5820-890-20-90](TB11-5820-890-20-90.pdf) | 1993-09-01
SINCGARS and EPLRS                                  | [TB 11-5820-1290-23](TB11-5820-1290-23.pdf)     | 2009-07-31
Dual SINCGARS and EPLRS with Overcab Antenna        | [TB 11-5820-1292-23](TB11-5820-1292-23.pdf)     | 2009-07-31
Dual SINCGARS and EPLRS with Curbside Antenna Tower | [TB 11-5820-1294-23](TB11-5820-1294-23.pdf)     | 2009-07-31

