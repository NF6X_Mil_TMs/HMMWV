# Point to Point Program

This is an interactive wiring and plumbing diagram which was provided to help maintenance personnel understand the M998 series trucks' complex systems. It runs under older versions of Windows. It seems to work in Wine.

After installation, run `C:\Program Files\TACOM_PTP\HMMWV\runa5w32.exe` and select the `hmvNAV.a5r` file.